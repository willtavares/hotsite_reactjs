from django.db import models

# Create your models here.
class backend(models.Model):
    titulo = models.CharField(max_length=100)
    descricao = models.CharField(max_length=300)

    def __str__(self):
        return self.titulo

    def __unicode__(self):
        return self.titulo
