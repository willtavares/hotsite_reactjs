from django.shortcuts import render
from rest_framework import viewsets, permissions
from .models import backend
from .serializers import backendSerializer
# Create your views here.

class backendView(viewsets.ModelViewSet):
    queryset = backend.objects.all()
    serializer_class = backendSerializer


