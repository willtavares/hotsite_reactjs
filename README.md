# PROVA DE FRONTEND & BACKEND

# FUNCIONAMENTO

    -O conceito proposto foi compor o ambiente em Django como backend e a Library ReactJS como frontend, de forma distinta.
    O backend, responsável pela administração do conteudo, Restfull.
    Frontend, cliente da API.

***********************************
Quero deixar destacado que o projeto apresentado não se assemelha completamente com o modelo real. Onde o mesmo é somente um conceito, com o intuito em demonstrar o conhecimento assim solicitado.

 Cada desenvolvimento em um critério especifico para atende-lo. Portanto, dentro da proposta, utilizei conforme solicitado o framework Django 2.0 para a composição do Backend, mais seus pacotes complementares como facilidadores, cujo foram:
 * django-cors-headers==2.2.0 // Para a requisição Cross Origin para o cliente Rest
 * djangorestframework==3.7.7 // Host Rest mais conhecido e completo para o Framwork
 * lazy-object-proxy==1.3.1 // Para a dinamização dos proxys durante a inicialização do Host e Cliente
 * freeze==1.0.10 // Responsável pela guarda futura de instalação Pacotes existentes no projeto

 + Os demais pacotes são doravantes do framework e do ambiente de desenvolvimento.
 + Banco de dados nativo do Framework sqlite3

 Para o ambiente Frontend, escolhi a Library ReactJS, pela complexidade do desafio e dinamização dos seus componentes. Tematizando com a escolha utilizada pela empresa exemplo do Hotsite.

 Para gerenciamento de pacotes escolhi o Yarn, por ser o mais adequado em atender o ReactJS.
 O Webpack já vem instalado como padrão nesta versão do ReactJS. Basta aciona-lo com yarn eject.
 O Bootstrap foi usado para lidar com o critério de responsividade.
 Sass para compor a personalização complementar.
 react-slick e slick-carousel na construção do carousel.

 Deixo aqui meu agradecimento pelo desafio e me encontro à disposição para futuras consultas.

 
 # INSTALAÇÂO

                ++ BACKEND ++

        Construa a Virtual VirtualEnvoirement da versão Python 3 ou posterior.
    -Link com as instruções: https://docs.python.org/3/tutorial/venv.html
    -Ative o Virtual VirtualEnvoirement (venv)
    -Retorne a pasta do projeto "hotsite_react"
    -Reinstale os pacotes: pip install -r requirements.txt

                ++ FRONTEND ++
        
        Instale o nodeJS. Instruções em: https://nodejs.org/en/download/
    -Instale a Biblioteca ReactJS: npm install -g create-react-app
    -Instale o gerenciador de pacotes yarn: npm install -g yarn
    -Reinstale as dependencias: yarn install

 # EXECUÇÂO

                ++ BACKEND ++

    -Dentro da pasta do projeto execute: python manage.py runserver

    *** Acesso ao Admin **
    Login: admin
    Senha: 123admin

                ++ FRONTEND ++
    
    -Dentro da pasta frontend execute o comando: yarn start