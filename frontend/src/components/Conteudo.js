import React from 'react';
import axios from 'axios';
import style from '../scss/conteudo.scss';

export default class Conteudo extends React.Component{

    constructor(){
        super();
        this.state = {
            materias: [],
        };
    }


componentDidMount(){
    fetch('http://127.0.0.1:8000/backend/')
    .then(response => response.json())
    .then(materias => { this.setState({materias:materias});
    })
    .catch(err => {
        console.log('Ocorreu um erro durante o fetch', err);
    });
}


    render(){
        return(
        <div class="container-fluid">
        {this.state.materias.map(item => (
                <div key={item.id} class="card size col-md-6 offset-md-4">
                    <div class="card-body">
                        <h5 class="card-title">{item.titulo}</h5>
                        <p class="card-text">{item.descricao}</p>
                    </div>
                </div>
          ))}
          </div>
        );
    }
}