import React from 'react';
import style from '../scss/cabecalho.scss';
// import hamburger from '../img/hamburger.png';

export default class Cabecalho extends React.Component{
    render(){
        return(
            <div class="pos-f-t">
                <div class="collapse" id="navbarToggleExternalContent">
                    <div class="bg-dark p-4">
                    <h4 class="text-white">Collapsed content</h4>
                    <span class="text-muted">Toggleable via the navbar brand.</span>
                    </div>
                </div>
                <nav class="navbar navbar-dark navbar_custom">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="col-md-7 offset-md-2 col-sm-6 titulo">
                        <p class="h1">COMBATE</p>
                    </div>
                </nav>
            </div>
        );
    }
}