import React from 'react';
import Slider from 'react-slick';
import style from '../scss/rodape.scss';

import mma1 from '../../src/img/mma1-min.jpg';
import mma2 from '../../src/img/mma2-min.jpg';
import mma3 from '../../src/img/mma3-min.jpg';
import mma4 from '../../src/img/mma4-min.jpg';
import mma5 from '../../src/img/mma5-min.jpg';



export default class Carrossel extends React.Component{

    render(){
        var settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            slidesToScroll: 1,
            autoplay: true,
            autoplaySpeed: 4000,
          };

        return(
            <div class="container-fluid">
                <div class="row" >
                    <div class="col-md-10 offset-md-2">
                        <Slider {...settings}>
                        <div ><img class="img-fluid" src={mma1}/></div>
                        <div ><img class="img-fluid" src={mma2}/></div>
                        <div ><img class="img-fluid" src={mma3}/></div>
                        <div ><img class="img-fluid" src={mma4}/></div>
                        <div ><img class="img-fluid" src={mma5}/></div>
                        </Slider>
                    </div>
                </div>    
            </div>
        );
    }
}