import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';


import Cabecalho from '../src/components/Cabecalho.js';
import Carrossel from '../src/components/Carrossel.js';
import Conteudo from '../src/components/Conteudo.js';
import Rodape from '../src/components/Rodape.js';

class App extends Component {
  render() {
    return (
      <div class="containder-fluid">
      <Cabecalho/>
      <Carrossel/>
      <Conteudo/>
      <Rodape/>
      </div>
    );
  }
}

export default App;
